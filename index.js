const express = require("express");
const app = express();
const port = 8000;
const drinkRouter = require("./app/routes/drinkRouter");
const voucherRouter = require("./app/routes/voucherRouter");
const userRouter = require("./app/routes/userRouter");
const orderRouter = require("./app/routes/orderRouter");
app.use((req, res, next) => {
    let today = new Date();
    console.log("Current: ", today);
    next();
  });
app.use((req, res, next) => {
    console.log(req.method);
    next();
});
  
app.get("/", (req, res) => {
    let today = new Date();
  
    res.json({
      message: `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${
        today.getMonth() + 1
      } năm ${today.getFullYear()}`,
    });
  });
  
  app.use(drinkRouter);
  app.use(voucherRouter);
  app.use(userRouter);
  app.use(orderRouter);
  app.listen(port, () => {
    console.log("App listening on port: ", port);
  });
  