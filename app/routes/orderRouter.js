const express = require("express");
const router = express.Router();

router.get("/orders", (req, res) => {
    res.status(200).json({
        orders: "Get all orders", 
    })
})
router.get("/orders/:orderId", (req, res)=>{
    let orderId = req.params.orderId;
    res.status(200).json({
        orders: "GET order Id = " + orderId,
    })
})
router.post("/orders", (req, res) => {
    res.status(200).json({
        orders: "Greate new order", 
    })
})

router.put("/orders/:orderId", (req, res)=>{
    let orderId = req.params.orderId;
    res.status(200).json({
        orders: "Update order Id = " + orderId,
    })
})
router.delete("/orders/:orderId", (req, res)=>{
    let orderId = req.params.orderId;
    res.status(200).json({
        orders: "Delete order Id = " + orderId,
    })
})
module.exports = router;