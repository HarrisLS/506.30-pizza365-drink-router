const express = require("express");
const router = express.Router();

router.get("/vouchers", (req, res) => {
    res.status(200).json({
        vouchers: "Get all vouchers", 
    })
})
router.get("/vouchers/:voucherId", (req, res)=>{
    let voucherId = req.params.voucherId;
    res.status(200).json({
        vouchers: "GET voucher Id = " + voucherId,
    })
})
router.post("/vouchers", (req, res) => {
    res.status(200).json({
        vouchers: "Greate new voucher", 
    })
})

router.put("/vouchers/:voucherId", (req, res)=>{
    let voucherId = req.params.voucherId;
    res.status(200).json({
        vouchers: "Update voucher Id = " + voucherId,
    })
})
router.delete("/vouchers/:voucherId", (req, res)=>{
    let voucherId = req.params.voucherId;
    res.status(200).json({
        vouchers: "Delete voucher Id = " + voucherId,
    })
})
module.exports = router;