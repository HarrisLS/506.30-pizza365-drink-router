const express = require("express");
const router = express.Router();

router.get("/users", (req, res) => {
    res.status(200).json({
        users: "Get all users", 
    })
})
router.get("/users/:userId", (req, res)=>{
    let userId = req.params.userId;
    res.status(200).json({
        users: "GET User Id = " + userId,
    })
})
router.post("/users", (req, res) => {
    res.status(200).json({
        users: "Greate new User", 
    })
})

router.put("/users/:userId", (req, res)=>{
    let userId = req.params.userId;
    res.status(200).json({
        users: "Update User Id = " + userId,
    })
})
router.delete("/users/:userId", (req, res)=>{
    let userId = req.params.userId;
    res.status(200).json({
        users: "Delete User Id = " + userId,
    })
})
module.exports = router;