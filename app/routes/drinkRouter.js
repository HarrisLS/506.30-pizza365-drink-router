const express = require("express");
const router = express.Router();

router.get("/drinks", (req, res) => {
    res.status(200).json({
        drinks: "Get all drinks", 
    })
})
router.get("/drinks/:drinkId", (req, res)=>{
    let drinkId = req.params.drinkId;
    res.status(200).json({
        drinks: "GET Drink Id = " + drinkId,
    })
})
router.post("/drinks", (req, res) => {
    res.status(200).json({
        drinks: "Greate new Drink", 
    })
})

router.put("/drinks/:drinkId", (req, res)=>{
    let drinkId = req.params.drinkId;
    res.status(200).json({
        drinks: "Update Drink Id = " + drinkId,
    })
})
router.delete("/drinks/:drinkId", (req, res)=>{
    let drinkId = req.params.drinkId;
    res.status(200).json({
        drinks: "Delete Drink Id = " + drinkId,
    })
})
module.exports = router;